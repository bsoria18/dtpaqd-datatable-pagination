
package es.lacaixa.absiscloud.abaaqd.common.util;

import java.io.IOException;
import java.io.Serializable;
import java.util.List;

import org.codehaus.jackson.JsonGenerationException;
import org.codehaus.jackson.map.JsonMappingException;
import org.codehaus.jackson.map.ObjectMapper;

/**
 * 
 * @author Bryan Soria
 *
 */
public class DataTableResponse implements Serializable {
	
	private static final long serialVersionUID = 1L;

	private int iTotalRecords;//Total records, before filtering (i.e. the total number of records in the database)

	private int iTotalDisplayRecords;//Total records, after filtering (i.e. the total number of records after filtering has been applied - not just the number of records being returned in this result set)

	private int sEcho;//An unaltered copy of sEcho sent from the client side.

	private List<?> data;//The data in a 2D array
	
	/**
	 * Datatable Response constructor
	 * @param iTotalRecords
	 * @param iTotalDisplayRecords
	 * @param sEcho
	 * @param data
	 */
	public DataTableResponse(int iTotalRecords, int iTotalDisplayRecords, int sEcho, List<?> data) {
		super();
		this.iTotalRecords = iTotalRecords;
		this.iTotalDisplayRecords = iTotalDisplayRecords;
		this.sEcho = sEcho;
		this.data = data;
	}

	public int getiTotalRecords() {
		return iTotalRecords;
	}

	public void setiTotalRecords(int iTotalRecords) {
		this.iTotalRecords = iTotalRecords;
	}

	public int getiTotalDisplayRecords() {
		return iTotalDisplayRecords;
	}

	public void setiTotalDisplayRecords(int iTotalDisplayRecords) {
		this.iTotalDisplayRecords = iTotalDisplayRecords;
	}

	public int getsEcho() {
		return sEcho;
	}

	public void setsEcho(int sEcho) {
		this.sEcho = sEcho;
	}

	public List<?> getData() {
		return data;
	}

	public void setData(List<?> data) {
		this.data = data;
	}
	
	public String toJson() {
		ObjectMapper objectMapper = new ObjectMapper();
		String json = null;
		try {
			json = objectMapper.writeValueAsString(this);
		} catch (JsonGenerationException e) {
			//Logger here
		} catch (JsonMappingException e) {
			//Logger here
		} catch (IOException e) {
			//Logger here
		}
		
		return json;
	}

}
