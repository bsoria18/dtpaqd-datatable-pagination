package es.lacaixa.absiscloud.abaaqd.common.util;

import java.io.Serializable;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

/**
 * 
 * @author Bryan Soria
 *
 */
public class DataTableRequest implements Serializable {

	private static final long serialVersionUID = 7597167471058927902L;

	private Integer iDisplayStart;

	private Integer iDisplayLength;

	private Integer iColumns;

	private List<String> sColumns;

	private String sSearch;

	private Boolean bRegex;

	private Integer iSortingCols;

	private Integer sEcho;

	private Integer sortedColumnNumber;

	private String sortedColumnName;

	private String sortedColumnDirection;
	
	private HashMap<String, String> sSearchPerColumn = new HashMap<String, String>();


	public Integer getiDisplayStart() {
		return iDisplayStart;
	}


	public void setiDisplayStart(Integer iDisplayStart) {
		this.iDisplayStart = iDisplayStart;
	}
	

	public Integer getiDisplayLength() {
		return iDisplayLength;
	}
	

	public void setiDisplayLength(Integer iDisplayLength) {
		this.iDisplayLength = iDisplayLength;
	}
	

	public Integer getiColumns() {
		return iColumns;
	}
	

	public void setiColumns(Integer iColumns) {
		this.iColumns = iColumns;
	}
	

	public String getsSearch() {
		return sSearch;
	}


	public void setsSearch(String sSearch) {
		if(sSearch != null) {
			this.sSearch = sSearch.toUpperCase();
		}else {
			this.sSearch = sSearch;
		}
	
	}


	public Boolean getbRegex() {
		return bRegex;
	}


	public void setbRegex(Boolean bRegex) {
		this.bRegex = bRegex;
	}


	public Integer getiSortingCols() {
		return iSortingCols;
	}


	public void setiSortingCols(Integer iSortingCols) {
		this.iSortingCols = iSortingCols;
	}


	public Integer getsEcho() {
		return sEcho;
	}


	public void setsEcho(Integer sEcho) {
		this.sEcho = sEcho;
	}


	public Integer getSortedColumnNumber() {
		return sortedColumnNumber;
	}


	public void setSortedColumnNumber(Integer sortedColumnNumber) {
		this.sortedColumnNumber = sortedColumnNumber+1;
	}


	public String getSortedColumnName() {
		return sortedColumnName;
	}


	public void setSortedColumnName(String sortedColumnName) {
		this.sortedColumnName = sortedColumnName;
	}


	public String getSortedColumnDirection() {
		return sortedColumnDirection;
	}


	public void setSortedColumnDirection(String sortedColumnDirection) {
		this.sortedColumnDirection = sortedColumnDirection;
	}

	public List<String> getsColumns() {
		return sColumns;
	}

	public void setsColumns(List<String> sColumns) {
		this.sColumns = sColumns;
	}
	
	public HashMap<String, String> getsSearchPerColumn() {
		return sSearchPerColumn;
	}


	public void setsSearchPerColumn(HashMap<String, String> sSearchPerColumn) {
		this.sSearchPerColumn = sSearchPerColumn;
	}
	
	
	/**
	 * DataTable constructor to load request parameters
	 * @param request
	 */
	public DataTableRequest(HttpServletRequest request) {

		String iDisplayLength = "iDisplayLength";
		String iDisplayStart = "iDisplayStart";
		String iColumns = "iColumns";
		String sSearch = "sSearch";
		String bRegex = "bRegex";
		String iSortingCols = "iSortingCols";
		String sEcho = "sEcho";
		String iSortCol_0 = "iSortCol_0";
		String sSortDir_0 = "sSortDir_0";
		String sColumns = "sColumns";

		
		Map<String, String> allRequestParams = new HashMap<String, String>();//Load parameters
		if(request.getParameterMap().size() > 0) {				
			Map<String, String[]> parameters = request.getParameterMap();
			for(String key : parameters.keySet()) {
				String[] vals = parameters.get(key);
				for(String val : vals)
					allRequestParams.put(key, val);
			}
		}     

		if (null != allRequestParams.get(bRegex)) {
			setbRegex(new Boolean(allRequestParams.get(bRegex)));
		}
		if (null != allRequestParams.get(iColumns)) {
			setiColumns(new Integer(allRequestParams.get(iColumns)));
		}
		if (null != allRequestParams.get(iDisplayLength)) {//Display in the current draw. It is expected that the number of records returned will be equal to this number, unless the server has fewer records to return.
			setiDisplayLength(new Integer(allRequestParams.get(iDisplayLength)));
		}
		if (null != allRequestParams.get(iDisplayStart)) {//Display start point in the current data set
			setiDisplayStart(new Integer(allRequestParams.get(iDisplayStart)));
		}
		if (null != allRequestParams.get(iSortingCols)) {
			setiSortingCols(new Integer(allRequestParams.get(iSortingCols)));
		}
		if (null != allRequestParams.get(sEcho)) {
			try {
				setsEcho(new Integer(allRequestParams.get(sEcho)));
			} catch (Exception e) {
			}
		}
		if (null != allRequestParams.get(sSearch)) {//Global search field
			setsSearch(allRequestParams.get(sSearch));
		}
		if (null != allRequestParams.get(iSortCol_0)) {//Column being sorted on
			setSortedColumnNumber(new Integer(allRequestParams.get(iSortCol_0))-1);
		}
		if (null != allRequestParams.get(sSortDir_0)) {//Direction to be sorted - "desc" or "asc".
			setSortedColumnDirection(allRequestParams.get(sSortDir_0));
		}
		if(null != allRequestParams.get(sColumns)) {
			List<String> columns = Arrays.asList((allRequestParams.get(sColumns).split( "[, \t\n\r]+" )));
			setsColumns(columns);//List of column names on database	
		}		
		List<String> columns = Arrays.asList((allRequestParams.get(sColumns).split( "," )));
		if (null != columns.get(getSortedColumnNumber())) {
			setSortedColumnName(columns.get(getSortedColumnNumber()));//Get Sort column name
		}	
		for(int i = 0 ; i < getiColumns(); i ++) {//Per column search field
			String keyForName = "sSearch_" + i;
	        if (!allRequestParams.get(keyForName).equals("") && allRequestParams.get(keyForName) != null ) {
	        	if(getsColumns().get(i+1) != null) {
	        		getsSearchPerColumn().put(getsColumns().get(i+1), allRequestParams.get(keyForName));
	        	}
	        	
	        }
		}
	}	
}
